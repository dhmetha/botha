const fs = require('fs');

const { Client, Collection, RichEmbed } = require('discord.js');
const bot = new Client();
bot.commands = new Collection();
bot.aquapls = false;
bot.config = require('./config.json');

const watcher = new (require('feed-watcher'))(bot.config.feed, 10);

const cheerio = require('cheerio');
const https = require('https');

fs.readdir('./events/', (err, files) => {
  if (err) return console.error(err);

  console.log(`loading ${files.length} events...`);

  files.forEach(f => {
  if (f.startsWith('_') || !f.endsWith('.js')) return;

  const event = require(`./events/${f}`);
  bot.on(f.split('.')[0], event.bind(null, bot));

  console.log(`loaded: ${f.split('.')[0]}`);
  });

  console.log();
});

fs.readdir('./commands/', (err, files) => {
  if(err) console.error(err);
  
  console.log(`loading ${files.length} commands...`);

  files.forEach((f) => {
  if (f.startsWith('_') || !f.endsWith('.js')) return;

  const cmd = require(`./commands/${f}`);

  for (let alias of cmd.aliases) {
    bot.commands.set(alias, cmd);
  }

  console.log(`loaded: ${f.split('.')[0]}`);
  });

  console.log();
});

watcher.on('new entries', function (entries) {
  entries.forEach(function (entry) {
    console.log(entry.title)
  })
})

watcher.on('new entries', (entries) => {
  entries.forEach((entry) => {
    console.log(`-- New Deal - ${entry.title} --`);

    https.get((entry.link), (res) => {
      let data = '';

      res.on('data', (chunk) => {
        data += chunk.toString();
      });

      res.on('end', () => {
        data = data.replace(/(\r|\n|\t)/gm, '');

        const $ = cheerio.load(data);
        const item = cheerio.load($('.threadItem').first().html());

        let price = item('.thread-price').text();
        let old_price = item('.mute--text').text().replace('@', '');
        let ship_price = item('.cept-shipping-price').text().replace('&#x20AC;', '€').replace(/ |Gratuit/g, '');
        let merchant = item('.cept-merchant-name').text();

        let description = `**${price}**`;
        if (old_price) description += ` ~~${old_price}~~`;
        if (ship_price) description += ` - :truck: **${ship_price}**`;
        if (merchant) description += ` - **${merchant}**`;

        let embed = new RichEmbed()
          .setAuthor(bot.user.username, bot.user.avatarURL)
          .setTitle(entry.title)
          .setDescription(description)
          .setURL(entry.guid)
          .setThumbnail(entry['media:content']['@'].url);

        bot.config.rss.forEach((chanid) => {
          let chan = bot.channels.get(chanid);
          embed.setColor(chan.guild.members.get(bot.user.id).displayColor);
          chan.send({embed});
        });
      });

      res.on('error', (err) => {
        console.log(err)
      });
    });
  });
});

watcher
  .start()
  .then(() => { console.log('-- Watcher ON --'); })
  .catch((err) => { console.error(err); });

bot.login(bot.config.token);
