const { RichEmbed } = require("discord.js");
const fs = require('fs');

exports.run = (bot, msg, args) => {
  if (msg.author.id != bot.config.ownerid) return;
  
  let index = bot.config.logs[msg.guild.id].indexOf(msg.channel.id);
  
  let embed = new RichEmbed()
    .setAuthor(bot.user.username, bot.user.avatarURL)
    .setColor(msg.guild.me.displayColor);
  
  switch (args[0].toLowerCase()) {
    case 'on':
      if (index == -1) {
        if (!bot.config.logs[msg.guild.id]) bot.config.logs[msg.guild.id] = [];
        
        bot.config.logs[msg.guild.id].push(msg.channel.id);

        fs.writeFile('config.json', JSON.stringify(bot.config), (err) => {
          if (err) throw err;
        });

        embed.setTitle(`Added \`#${msg.channel.name}\``);
      }
      else embed.setTitle(`\`#${msg.channel.name}\` is already ON`);
      break;
    
    case 'off':
      if (index != -1) {
        bot.config.logs[msg.guild.id].splice(index, 1);
        embed.setTitle(`Removed \`#${msg.channel.name}\``);
        
        fs.writeFile('config.json', JSON.stringify(bot.config), (err) => {
          if (err) throw err;
        });
      }
      else embed.setTitle(`\`#${msg.channel.name}\` is already OFF`);
      break;
    
    default:
      embed.setTitle('Choose \'on\' or \'off\' for this channel');
      break;
  }
  
  msg.channel.send({embed});
};

exports.admin = true;
exports.usage = "logs [on|off]";
exports.aliases = ["logs"];
exports.help = "Turns on or off the edit & delete logs in the channel";
