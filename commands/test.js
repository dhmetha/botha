const cheerio = require('cheerio');
const https = require('https');

exports.run = (bot, msg, args) => {
  bot.embed = null;
  if (msg.author.id != bot.config.ownerid) return;

  https.get(('https://www.dealabs.com/bons-plans/kit-5-outils-bosch-pro-12v-perceuse-visseuse-visseuse-a-chocs-decoupeur-ponceur-scie-sabre-lampe-chargeur-3-batteries-2ah-1643597'), (res) => {
    let data = '';
    let avURL = msg.author.avatarURL;
    if (avURL.endsWith(".gif")) avURL += "?size=2048";

    res.on('data', (chunk) => {
      data += chunk.toString();
    });

    res.on('end', () => {
      data = data.replace(/(\r|\n|\t)/gm, '');

      const $ = cheerio.load(data);

      const item = cheerio.load($('.threadItem').first().html());

      let title = item('.thread-title--item').text();
      let price = item('.thread-price').text();
      let old_price = item('.mute--text').text().replace('@', '');
      let ship_price = item('.cept-shipping-price').text().replace('&#x20AC;', '€').replace(/ |Gratuit/g, '');
      let merchant = item('.cept-merchant-name').text();

      // console.log(item('.cept-shipping-price').html());

      let description = `**${price}** *~~${old_price}~~*`;

      if (ship_price) description += ` :truck: ${ship_price}`;
      if (merchant) description += ` - ${merchant}`;

      console.log(title);
      console.log(description);
    });
  }).on("error", (e) => {
    console.log("Error:");
    console.error(e);
  });
};

exports.admin = true;
exports.usage = "test";
exports.aliases = ["test"];
exports.help = "help_message";
