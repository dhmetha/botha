exports.run = (bot, msg, args) => {
  if (msg.author.id != bot.config.ownerid) return;

  let text = '```';
  
  for (let member of msg.guild.members) {
    text += `${member[1].user.id} : ${member[1].displayName}\n`;
  }

  text += '```';

  msg.author.send(text);
};

exports.admin = true;
exports.usage = "members";
exports.aliases = ["members"];
exports.help = "Send in DM members of this guild";
