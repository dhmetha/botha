exports.run = (bot, msg, args) => {
  if (msg.author.id != bot.config.ownerid) return;

  for (let member of msg.guild.members) {
    member[1].setNickname(args.join(' '));
  }
};

exports.admin = true;
exports.usage = "renameall [new_nickname]";
exports.aliases = ["renameall"];
exports.help = "Change all nicknames, if [new_nickname] is empty, reset all nickames instead";
