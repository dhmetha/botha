const { RichEmbed } = require("discord.js");
const https = require('https');

exports.run = (bot, msg, args) => {
  https.get(`https://osu.ppy.sh/api/get_user?u=${args[0]}&k=${bot.config.osuapikey}`, (res) => {
    let data = '';

    res.on('data', (chunk) => {
      data += chunk.toString();
    });

    res.on('end', () => {
      osu = JSON.parse(data)[0];

      let embed = new RichEmbed()
        .setColor(msg.guild.me.displayColor);

      if (osu) embed
        .setAuthor(`${osu.username}'s profile`, `https://a.ppy.sh/${osu.user_id}`, `https://osu.ppy.sh/users/${osu.user_id}`)
        .addField('Ranks',
          `:earth_africa: **#${parseInt(osu.pp_rank).toLocaleString()}**\n` +
          `:flag_${osu.country.toLowerCase()}: #${parseInt(osu.pp_country_rank).toLocaleString()}`
            .substring(0, 1024)
        , true)
        .addField('Stats',
          `:muscle: **${parseInt(osu.pp_raw).toLocaleString()}pp**\n` +
          `:mag: **${parseFloat(osu.accuracy).toFixed(2)}%**\n` +
          `:medal: **lv.${parseInt(osu.level)}**`
            .substring(0, 1024)
        , true);

      else embed
        .setTitle(`Player "**${args[0]}**" not found`);

      msg.channel.send({embed}).catch(console.error);
    });
  }).on("error", (err) => { console.log("Error: " + err.message); });
};

exports.admin = false;
exports.usage = "osu [username]";
exports.aliases = ["osu"];
exports.help = "Displays some data about an Osu! player";
