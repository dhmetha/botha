exports.run = (bot, msg, args) => {
  if (msg.author.id != bot.config.ownerid) return;

  let text = tmp = '';

  for (let guild of bot.guilds) {
    tmp = text + `${guild[1].id},${guild[1].name}\n`;
    if (tmp.length > 2000) {
      msg.author.send(text);
      text = '';
    }
    text += `${guild[1].id},${guild[1].name}\n`;
  }

  msg.author.send(text);
};

exports.admin = true;
exports.usage = "guilds";
exports.aliases = ["guilds"];
exports.help = "Send in DM all guild names of the bot";
