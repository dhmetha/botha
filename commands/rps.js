const { RichEmbed } = require("discord.js");
const { MessageCollector } = require("discord.js");

exports.run = (bot, msg, args) => {
  let embed = new RichEmbed()
    .setAuthor(bot.user.username, bot.user.avatarURL)
    .setColor(msg.guild.me.displayColor);
  
  if (msg.mentions.members.first() != undefined) {
    if (!msg.mentions.members.first().user.bot) {
      let p1 = msg.member;
      let p2 = bot.guilds.get(msg.guild.id).members.get(msg.mentions.members.first().user.id);
      
      p1.user.createDM().then(function(dm1) {
        p2.user.createDM().then(function(dm2) {
          embed
            .setTitle(`RPS game: **${p1.displayName}** vs. **${p2.displayName}**`)
            .setDescription('Waiting for the choices\nPlayers, DM me your choice between \'R\', \'P\' or \'S\'\n(**R**ock, **P**aper or **S**cissors)');
          
          msg.channel.send({embed}).then(() => {
            let game = bot.user.lastMessage;
            let collector1 = new MessageCollector(dm1, m => m == m, { time: 10000 });
            let collector2 = new MessageCollector(dm2, m => m == m, { time: 10000 });
            let i1 = 0;
            let i2 = 0;
            let rps = ['r', 'p', 's'];
            
            p1.user.send(`RPS game VS ${p2.displayName}\nPlease choose between \'R\', \'P\' or \'S\'\n(**R**ock, **P**aper or **S**cissors)`);
            p2.user.send(`RPS game VS ${p1.displayName}\nPlease choose between \'R\', \'P\' or \'S\'\n(**R**ock, **P**aper or **S**cissors)`);
            
            collector1.on('collect', mess => {
              if (rps.indexOf(mess.content.toLowerCase()) > -1) {
                p1.rps = mess.content.toLowerCase();
                mess.author.send(`Nice choice\ngo back to the chan here: <#${msg.channel.id}>`);
                if (i2 == 1) collector1.stop();
                i1 = 1;
              }
              else mess.author.send('Please choose between \'R\', \'P\' or \'S\'\n(**R**ock, **P**aper or **S**cissors)');
            });
            
            collector2.on('collect', mess => {
              if (rps.indexOf(mess.content.toLowerCase()) > -1) {
                p2.rps = mess.content.toLowerCase();
                mess.author.send(`Nice choice\ngo back to the chan here: <#${msg.channel.id}>`);
                if (i1 == 1) collector2.stop();
                i2 = 1;
              }
              else mess.author.send('Please choose between \'R\', \'P\' or \'S\'\n(**R**ock, **P**aper or **S**cissors)');
            });
            
            collector1.on('end', () => {
              let embed = new RichEmbed()
                .setAuthor(bot.user.username, bot.user.avatarURL)
                .setColor(msg.guild.me.displayColor);
              
              if (i1 == 1 && i2 == 1) {
                if ((p1.rps == 'r' && p2.rps == 's') || (p1.rps == 's' && p2.rps == 'p') || (p1.rps == 'p' && p2.rps == 'r')) embed.setTitle(`${p1.displayName} Won !`);
                else if (p1.rps == p2.rps) embed.setTitle('It\'s a draw !');
                else embed.setTitle(`${p2.displayName} Won !`)
                
                embed.setDescription(`${p1.displayName}: **${p1.rps}**\n${p2.displayName}: **${p2.rps}**`);
              }
              else if (i1 == 1 && i2 == 0) embed.setTitle(`**${p1.displayName}** made his choice`);
              else if (i1 == 0) embed.setTitle('Someone didn\'t made his choice');
              
              game.edit({embed});
            });
            
            collector2.on('end', () => {
              let embed = new RichEmbed()
                .setAuthor(bot.user.username, bot.user.avatarURL)
                .setColor(msg.guild.me.displayColor);
              
              if (i2 == 1 && i1 == 1) {
                if ((p1.rps == 'r' && p2.rps == 's') || (p1.rps == 's' && p2.rps == 'p') || (p1.rps == 'p' && p2.rps == 'r')) embed.setTitle(`${p1.displayName} Won !`);
                else if (p1.rps == p2.rps) embed.setTitle('It\'s a draw !');
                else embed.setTitle(`${p2.displayName} Won !`)
                
                embed.setDescription(`${p1.displayName}: **${p1.rps}**\n${p2.displayName}: **${p2.rps}**`);
              }
              else if (i2 == 1 && i1 == 0) embed.setTitle(`**${p1.displayName}** made his choice`);
              else if (i2 == 0) embed.setTitle('Someone didn\'t made his choice');
              
              game.edit({embed});
            });
          }).catch(e => { console.error(e) });
        }).catch(e => { console.error(e) });
      }).catch(e => { console.error(e) });
    }
    else {
      embed.setTitle('You can\'t play VS a bot user, sorry');
      msg.channel.send({embed});
    }
  }
  else {
    embed.setTitle(`Please tag someone`);
    msg.channel.send({embed});
  }
};

exports.admin = false;
exports.usage = "rps [@user]";
exports.aliases = ["rps"];
exports.help = "Start an RPS game vs the @user";
