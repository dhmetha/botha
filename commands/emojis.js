exports.run = (bot, msg, args) => {
  let text, tmp = '';

  for (let emoji of msg.guild.emojis) {
    tmp = text + `${emoji.toString()}\n`;
    if (tmp.length > 2000) {
      msg.author.send(text);
      text = '';
    }
    text += `${emoji.toString()}\n`;
  }

  msg.author.send(text);
};

exports.admin = false;
exports.usage = "emojis";
exports.aliases = ["emojis"];
exports.help = "Send in your DM all emojis of this guild";
