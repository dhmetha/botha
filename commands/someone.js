const { RichEmbed } = require("discord.js");

exports.run = (bot, msg, args) => {
  let embed = new RichEmbed()
    .setAuthor(bot.user.username, bot.user.avatarURL)
    .setColor(msg.guild.me.displayColor)
    .setDescription(`<@${msg.guild.members.random().user.id}> ${args.join(' ')}`);

  msg.channel.send({embed});
};

exports.admin = false;
exports.usage = "someone [nice_message]";
exports.aliases = ["someone"];
exports.help = "Say something to a random user";
