const { RichEmbed } = require("discord.js");

exports.run = (bot, msg, args) => {
  if (msg.author.id != bot.config.ownerid) return;

  let avURL = msg.author.avatarURL;
  if (avURL.endsWith(".gif")) avURL += "?size=2048";

  let embed = new RichEmbed()
    .setAuthor(msg.member.displayName, avURL)
    .setColor(msg.member.displayColor);
  
  msg.channel.send({embed});
};

exports.admin = true;
exports.usage = "base";
exports.aliases = ["base"];
exports.help = "help_message";
