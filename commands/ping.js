const { RichEmbed } = require("discord.js");

exports.run = (bot, msg, args) => {
  let embed = new RichEmbed()
    .setAuthor(bot.user.username, bot.user.avatarURL)
    .setColor(msg.guild.me.displayColor)
    .setTitle('Pong!');

  msg.channel.send({embed});
};

exports.admin = false;
exports.usage = "ping";
exports.aliases = ["ping"];
exports.help = "Ping?";
