const { RichEmbed } = require('discord.js');

exports.run = (bot, msg, args) => {
  let avURL = msg.author.avatarURL;
  if (avURL.endsWith('.gif')) avURL += '?size=2048';

  let reqembed = new RichEmbed()
    .setAuthor(msg.member.displayName, avURL)
    .setColor(msg.member.displayColor)
    .addField('Request', args.join(' ').substring(0, 1024))
    .addField('From', `<@${msg.author.id}>\n(${msg.author.id})`.substring(0, 1024), true)
    .addField('In', `<#${msg.channel.id}>\n(${msg.guild.name})`.substring(0, 1024), true);
    
  let embed = new RichEmbed()
    .setAuthor(bot.user.username, bot.user.avatarURL)
    .setColor(msg.guild.me.displayColor)
    .setTitle('Request successfully sent, thanks');

  bot.users.get(bot.config.ownerid).send({embed: reqembed});

  msg.channel.send({embed});
};

exports.admin = false;
exports.usage = "request [request_message]";
exports.aliases = ["request"];
exports.help = "Request something to the dev";
