exports.run = (bot, msg, args) => {
  if (bot.aquapls) bot.aquapls = false;
  else bot.aquapls = true;
};

exports.admin = false;
exports.usage = "aquapls";
exports.aliases = ["aquapls"];
exports.help = "Toggle aquapls react on all messages";
