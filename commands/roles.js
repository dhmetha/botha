exports.run = (bot, msg, args) => {
  let text = tmp = '';

  for (let role of msg.guild.roles) {
    tmp = text + `${role[1].id},${role[1].name}\n`;
    if (tmp.length > 2000) {
      msg.author.send(text);
      text = '';
    }
    text += `${role[1].id},${role[1].name}\n`;
  }

  msg.author.send(text);
};

exports.admin = true;
exports.usage = "roles";
exports.aliases = ["roles"];
exports.help = "Send in DM all roles of this guild";
