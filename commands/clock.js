exports.run = (bot, msg, args) => {
  msg.channel.send(':clock12:').then(() => {
    let clock = bot.user.lastMessage;
    let time = 0;

    for (let i = 1; i <= 12; i++) {
      time += 1000;

      setTimeout(() => {
        clock.edit(`:clock${i}:`);
      }, time);
    }
  }).catch(e => { console.error(e) });
};

exports.admin = false;
exports.usage = "clock";
exports.aliases = ["clock"];
exports.help = "Send a clock";
