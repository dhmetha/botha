const { RichEmbed } = require("discord.js");

exports.run = (bot, msg, args) => {
  msg.channel.fetchMessage(args[0]).then(m => {
    let embed = new RichEmbed()
      .setAuthor(m.member.displayName, m.author.avatarURL)
      .setColor(m.member.displayColor)
      .setDescription(m.content)
      .setTimestamp(m.createdTimestamp)
      .setFooter(`in #${m.channel.name}`);
  
    msg.channel.send({embed});
  }).catch(e => { console.error(e) });
};

exports.admin = false;
exports.usage = "quote [message_id]";
exports.aliases = ["quote"];
exports.help = "Quote a message by his ID";
