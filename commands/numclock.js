const { RichEmbed } = require("discord.js");

exports.run = (bot, msg, args) => {
  if (msg.author.id != bot.config.ownerid) return;
  
  let nums = [
    [
      '  ___  ',
      ' / _ \\ ',
      '| | | |',
      '| | | |',
      '| |_| |',
      ' \\___/ '
    ],
    [
      ' __ ',
      '/_ |',
      ' | |',
      ' | |',
      ' | |',
      ' |_|'
    ],
    [
      ' ___  ',
      '|__ \\ ',
      '   ) |',
      '  / / ',
      ' / /_ ',
      '|____|'
    ],
    [
      ' ____  ',
      '|___ \\ ',
      '  __) |',
      ' |__ < ',
      ' ___) |',
      '|____/ '
    ],
    [
      ' _  _   ',
      '| || |  ',
      '| || |_ ',
      '|__   _|',
      '   | |  ',
      '   |_|  '
    ],
    [
      ' _____ ',
      '| ____|',
      '| |__  ',
      '|___ \\ ',
      ' ___) |',
      '|____/ '
    ],
    [
      '   __  ',
      '  / /  ',
      ' / /_  ',
      '|  _ \\ ',
      '| (_) |',
      ' \\___/ '
    ],
    [
      ' ______ ',
      '|____  |',
      '  / / ',
      '   / /  ',
      '  / /   ',
      ' /_/  '
    ],
    [
      '  ___  ',
      ' / _ \\ ',
      '| (_) |',
      ' > _ < ',
      '| (_) |',
      ' \\___/ '
    ],
    [
      '  ___  ',
      ' / _ \\ ',
      '| (_) |',
      ' \\__, /',
      '   / / ',
      '  /_/  '
    ]
  ];

  let text = '';

  for (let i = 0; i < 6; i++) {
    for (let digit of args[0]) {
      text += nums[digit][i] + '   ';
    }
    text += '\n';
  }

  if (text[0] == ' ') text[0] = '.';

  console.log(text);
  
  let embed = new RichEmbed()
    .setAuthor(bot.user.username, bot.user.avatarURL)
    .setColor(msg.guild.me.displayColor)
    .setDescription(text);

  msg.channel.send(text).then(() => {
    console.log(bot.user.lastMessage.content);
  }).catch(e => { console.error(e) });
};

exports.admin = true;
exports.usage = "numclock";
exports.aliases = ["numclock"];
exports.help = "WIP command";
