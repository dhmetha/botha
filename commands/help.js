const { RichEmbed } = require("discord.js");
const fs = require('fs')

exports.run = (bot, msg, args) => {
  fs.readdir('./commands/', (err, files) => {
    if (err) console.error(err);

    let admin = (msg.member.hasPermission('ADMINISTRATOR') || msg.author.id == '188581786031226889' ? true : false);

    console.log(admin);

    let embed = new RichEmbed()
      .setAuthor(bot.user.username, bot.user.avatarURL)
      .setColor(msg.guild.me.displayColor);

    let index = -1;

    if (args[0]) index = files.indexOf(`${args[0].toLowerCase()}.js`);
    
    let cmd, name;
    
    if (index != -1) {
      cmd = require(`./${files[index]}`);
      name = cmd.aliases[0].charAt(0).toUpperCase() + cmd.aliases[0].slice(1);

      embed
        .setTitle(name)
        .setDescription(`Usage: ${bot.config.prefix}${cmd.usage}\`\`\`${cmd.help}\`\`\`*Aliases: ${cmd.aliases.join(', ')}*`);
    }
    else {
      embed.setTitle(`Bot commands (prefix: **${bot.config.prefix}**)`);
      
      let text = `Commands: **${this.aliases[0].charAt(0).toUpperCase()}${this.aliases[0].slice(1)}**`;
      let textadmin = text;
      
      files.forEach((f) => {
        if (f.startsWith('_') || !f.endsWith('.js')) return;
        
        cmd = require(`./${f}`);
        
        if (cmd == this) return;

        name = cmd.aliases[0].charAt(0).toUpperCase() + cmd.aliases[0].slice(1);
        
        if (cmd.admin) {
          if (admin) textadmin += `, **${name}**`;
          return;
        }

        text += `, **${name}**`;
      });

      if (admin) {
        embed.setDescription(textadmin);
        msg.author.send('Here are some additional admin commands', {embed});
      }

      embed.setDescription(text);
    }

    msg.channel.send({embed});
  });
};

exports.admin = false;
exports.usage = "help";
exports.aliases = ["help", "h"];
exports.help = "S.O.S";
