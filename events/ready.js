module.exports = (bot) => {
  console.log(`${bot.user.username} is [ON], with ${bot.users.size} users, in ${bot.guilds.size} guilds`);
  console.log('~');

  bot.user.setPresence({
    game: {
      name: `Type ${bot.config.prefix}help`,
      type: 'PLAYING'
    }
  });
};
