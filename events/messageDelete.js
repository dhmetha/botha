const { RichEmbed } = require('discord.js');

module.exports = (bot, msg) => {
  if (!msg.guild || msg.content.startsWith(bot.config.prefix)) return;
  if (msg.author.bot) return;
  if (!bot.config.logs[msg.guild.id]) return
  
  let embed = new RichEmbed()
    .setAuthor(msg.member.displayName, msg.author.avatarURL)
    .setColor(msg.member.displayColor)
    .setTitle(`Delete in #${msg.channel.name}`)
    .setDescription(msg.content);

  bot.config.logs[msg.guild.id].forEach((chanid) => {
    msg.guild.channels.get(chanid).send({embed});
  });
};
