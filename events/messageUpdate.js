const { RichEmbed } = require("discord.js");

module.exports = (bot, oldmsg, newmsg) => {
  if (!oldmsg.guild) return;
  if (oldmsg.content == newmsg.content || oldmsg.author.bot) return;
  if (!bot.config.logs[oldmsg.guild.id]) return
  
  let embed = new RichEmbed()
    .setAuthor(oldmsg.member.displayName, oldmsg.author.avatarURL)
    .setColor(oldmsg.member.displayColor)
    .setTitle(`Edit in #${oldmsg.channel.name}`)
    .addField('Before', oldmsg.content.substring(0, 1024))
    .addField('After', newmsg.content.substring(0, 1024));

  bot.config.logs[oldmsg.guild.id].forEach((chanid) => {
    oldmsg.guild.channels.get(chanid).send({embed});
  });
};
