const { RichEmbed } = require("discord.js");

module.exports = (bot, msg) => {
  if (bot.aquapls && !msg.content.startsWith(bot.config.prefix)) msg.react('514419038521131028');
  if (msg.author.bot || !msg.content.startsWith(bot.config.prefix) || !msg.guild) return;

  const args = msg.content.slice(bot.config.prefix.length).trim().split(/ +/g);
  const command = args.shift().toLowerCase();
  const cmd = bot.commands.get(command);

  let embed = new RichEmbed()
    .setAuthor(bot.user.username, bot.user.avatarURL)
    .setColor(msg.guild.me.displayColor);

  if (cmd) {
    if (cmd.admin && !msg.member.hasPermission('ADMINISTRATOR') && msg.author.id != '188581786031226889') msg.channel.send({embed: embed.setTitle('You can\'t use this command, sorry')});
    else cmd.run(bot, msg, args);
  } 
  else msg.channel.send({embed: embed.setTitle(`"**${bot.config.prefix}${command}**" ? I dont know this command, sorry`)});

  msg.delete().catch(console.error);
};
